// Initialize your app
var myApp = new Framework7({
    modalTitle:'Cofeine!'
});

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

myApp.onPageBeforeAnimation('map-page', function(page){
    myApp.hidePreloader();
});

myApp.onPageBeforeInit('map-page', function(page){
    myApp.showPreloader('Fetching locations!');
});

$$(document).on('pageInit', function (e) {
  // Get page data from event data
  var page = e.detail.page;

  if (page.name === 'map-page') {
    // Following code will be executed for page with data-page attribute equal to "about"
    //myApp.alert('Here comes About page', 'Cofeine!');
    currentPage = 'map-page';
    // draw map
    //drawMap();
    findCurrentLocation();
    //drawMap();
  }

  if (page.name === 'new-store') {
    // show form and load map for the upper part of the form
    currentPage = 'new-store';
    findCurrentLocation();
  }

  if (page.name === 'details'){
    currentPage = 'details';
    //getStoreDetails();
  }
});



function initMap(){
    myApp.alert('Here comes map', 'Cofeine!');
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
$$(document).on('click', '#signup-button', function(e){
    //e.preventDefault();
     var email = $$("#username-input").val();
     var password = $$("#password-input").val();
     var r_password = $$("#repeat-password-input").val();
     var valid_email = validateEmail(email)
    // console.log(email);
    // console.log(password);
    // console.log(r_password);

    if(valid_email)
    {
        if(password.length >= 6)
        {
               if(password === r_password){
                $$.ajax({
                    url: 'http://' + base_url + '/coffee_services/index.php/api/users',
                    type: 'POST',
                    data: { email: email,
                            nicename: password
                        },
                    success:function(){
                        myApp.alert('You registered successfully!');
                        mainView.router.loadPage('login.html');
                    },
                    fail:function(){
                        myApp.alert('Something went wrong, please try again later.');
                    }
                });
            }
            else
            {
                myApp.alert('Passwords seems to missmatch...');
            }
        }
        else
        {
            myApp.alert('Password must be at least six characters.');
        }
    }
    else
    {
        myApp.alert('Please check email, it seems not valid...');
    }

});

function onBackKey(){
    mainView.router.back();

}

document.addEventListener("backbutton", onBackKey, false);


// Generate dynamic page
var dynamicPageIndex = 0;
function createContentPage() {
	mainView.router.loadContent(
        '<!-- Top Navbar-->' +
        '<div class="navbar">' +
        '  <div class="navbar-inner">' +
        '    <div class="left"><a href="#" class="back link"><i class="icon icon-back"></i><span>Back</span></a></div>' +
        '    <div class="center sliding">Dynamic Page ' + (++dynamicPageIndex) + '</div>' +
        '  </div>' +
        '</div>' +
        '<div class="pages">' +
        '  <!-- Page, data-page contains page name-->' +
        '  <div data-page="dynamic-pages" class="page">' +
        '    <!-- Scrollable page content-->' +
        '    <div class="page-content">' +
        '      <div class="content-block">' +
        '        <div class="content-block-inner">' +
        '          <p>Here is a dynamic page created on ' + new Date() + ' !</p>' +
        '          <p>Go <a href="#" class="back">back</a> or go to <a href="services.html">Services</a>.</p>' +
        '        </div>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>'
    );
	return;
}
