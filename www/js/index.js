
// $$(document).on('click', '#fb-login-btn', function(){
//     facebookConnectPlugin.login(["public_profile"],
//         fbLoginSuccess,
//         function (error) { alert("" + error); }
//     );
// });

// var fbLoginSuccess = function (userData) {
//     alert("UserInfo: " + JSON.stringify(userData));
// }

$$( document ).on( "pageshow", "#home", function() {
    $$( "#autocomplete" ).on( "filterablebeforefilter", function ( e, data ) {
        var $ul = $$( this ),
            $input = $$( data.input ),
            value = $input.val(),
            html = "";
        var url = " "
        $ul.html( "" );
        if ( value && value.length > 2 ) {
            $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
            $ul.listview( "refresh" );

           geocodeAddress(value);

        }
    });
});


function geocodeAddress(value) {


  var geocoder = new google.maps.Geocoder();
  var address = document.getElementById('autocomplete').value;

  address = value;

  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {

        showResults(results);

    } else {
      console.log('error');
    }
  });

}


function showResults(results){

    $ul = $$("#autocomplete");
    html = "";
    $.each(results, function(index, result) {
        defaultLatLng = result.geometry.location;
        html += "<li data-rowno=" + index + ">" + result.formatted_address + "</li>";
        if (index == 2) return false;
    });
    $ul.html( html );
    $ul.listview( "refresh" );
    $ul.trigger( "updatelayout");

    $$('ul').on('click', 'li', function(event) {
        defaultLatLng = results[$$(this).data("rowno")].geometry.location;
        location.href = '#map-page';
    });

}

