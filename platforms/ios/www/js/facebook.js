var loggedIn = false;
var userID;
var favoriteStores;


$$(document).on('click', '#fb-login-btn', function(e){
  e.preventDefault();
  console.log('click');
  FB.login(function(response) {
    if (response.status === 'connected') {
      // Logged into your app and Facebook.

      loggedIn = true;
      myApp.alert('You are logged in successfuly!');

    } else if (response.status === 'not_authorized') {
      myApp.alert('Not authorized');
      // The person is logged into Facebook, but not your app.
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      myApp.alert('Not logged in')
    }
  });
  //checkLoginState();
});

  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      loggedIn = true;
      myApp.alert('You are logged in successfuly!');
      //testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is loginged into Facebook, but not your app.
      loggedIn = false;
      myApp.alert('Please log into this app.');

    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      loggedIn = false;
      myApp.alert('Please log into Facebook.');
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '188731468136811',//'446441558876308',
    cookie     : true,  // enable cookies to allow the server to access
                        // the session

    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.5' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');

    FB.api('/me', {fields: 'email,gender,first_name,name'}, function(response) {

      $$.ajax({
        url: 'http://localhost/coffee_services/index.php/api/users',
        type: 'POST',
        data: {
        nicename: response.name,
        email: response.email
      }
      })
      .done(function(data) {
        userID = parseInt(data.user_id);
        favoriteStores = data.favorite_stores;
        console.log(data);
        $$("#favorite-page-link").show();
        $$("#visited-page-link").show();

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });


      console.log(response);
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
