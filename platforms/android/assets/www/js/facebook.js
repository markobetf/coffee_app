var loggedIn = false;
var userID;
var favoriteStores;


$$(document).on('click', '#fb-login-btn', function(e){
  e.preventDefault();
  //console.log('click');
  facebookConnectPlugin.login(["public_profile","email","gender"],
      fbLoginSuccess,
      function (error) { alert("" + error); }
  );
});


var fbLoginSuccess = function (userData) {
    alert("UserInfo: " + JSON.stringify(userData));

    if (response.status === 'connected') {
      // Logged into your app and Facebook.

      loggedIn = true;
      myApp.alert('You are logged in successfuly!');
      getUserData();

    } else if (response.status === 'not_authorized') {
      myApp.alert('Not authorized');
      // The person is logged into Facebook, but not your app.
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      myApp.alert('Not logged in')
    }
}

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function getUserData() {
    console.log('Welcome!  Fetching your information.... ');

    facebookConnectPlugin.api('/me', {fields: 'email,gender,first_name,name'}, function(response) {

      $$.ajax({
        url: 'http://localhost/coffee_services/index.php/api/users',
        type: 'POST',
        data: {
        nicename: response.name,
        email: response.email
      }
      })
      .done(function(data) {
        userID = parseInt(data.user_id);
        favoriteStores = data.favorite_stores;
        console.log(data);
        $$("#favorite-page-link").show();
        $$("#visited-page-link").show();

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });


      console.log(response);
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    }
    ,
    function(error){
      myApp.alert(error);
    }

    );
  }
