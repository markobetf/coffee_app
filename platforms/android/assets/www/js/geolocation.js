var testLocations = [
['Location 1', 43.46419, -80.54095],
['Location 2', 43.469, -80.58091],
['Location 3', 43.465, -80.56098]
]

var markerPosition;
var defaultLatLng;
//id of current Coffee shop on the Info page
var currentID;
//distance of current Coffee shop on the Info page
var currentDistance;
var nearestLocations;
var currentReview;
var isReviewUpdate;
var newAddress;
//infoPopup tells wheter poup is new caffee(1), review(2) or rate(3)
var infoPopup;
var getStoresType;

// new vars

// we will set location undefined, and if findCurrentLocation is successfull then this will be false
var locationUndefined = true;

var map = null;

var base_url = '192.168.43.210';
var currentPage = 'main'; // keep track of current page name

var stores = null; // keep current store list in json format

var center = null; // keep center of current map view
var radius = null; // keep radius of current map bounds

function findCurrentLocation(){
    myApp.showPreloader('Getting your location!');
    navigator.geolocation.getCurrentPosition(onSuccess, onError, {timeout: 10000});
}

var onSuccess = function(position){
    myApp.hidePreloader();
    // location is found
    locationUndefined = false;
    defaultLatLng = {lat: position.coords.latitude, lng: position.coords.longitude}
    drawMap(defaultLatLng);
    //myApp.alert('Location found: ' + position.coords.latitude + ';' + position.coords.longitude, 'Cofeine!');
}

function onError(error){
    // console.log('code: '    + error.code    + '\n' +
    //       'message: ' + error.message + '\n');
    myApp.hidePreloader();
    myApp.alert('Please enable your location services!', 'Cofeine!');
}

/*
 * Google Maps documentation: http://code.google.com/apis/maps/documentation/javascript/basics.html
 * Geolocation documentation: http://dev.w3.org/geo/api/spec-source.html
 */
/*
$$( document ).on( "pageInit", '.page[data-page="map-page"]', function() {

    //var defaultLatLng = new google.maps.LatLng(34.0983425, -118.3267434);  // Default to Hollywood, CA when no geolocation support
        if(loggedIn) $$("#add-coffee-button").show();
        else $$("#add-coffee-button").hide();
        drawMap(defaultLatLng);

});
*/
$$( document ).on( "pageshow", "#favorite-page", function() {
        getStoresType = 'favorite';
        getStores(userID, getStoresType);
});

$$( document ).on( "pageshow", "#visited-page", function() {
        getStoresType = 'visited';
        getStores(userID, getStoresType);
});

$$( document ).on( "pageshow", "#list-page", function() {

    // $.each(nearestLocations, function(index, val) {
    //      $$("#coffee-list").append('<a href="#" class="ui-btn ui-corner-all">Info</a>');
    // });
    $$(".ui-btn.ui-corner-all.coffee-link").on('click', function(event) {
        event.preventDefault();
        currentID = $$(this).data("coffee-id");
        var latClicked = $$(this).data("coffee-lat");
        var lngClicked = $$(this).data("coffee-lng");
        markerPosition = {lat: parseFloat(latClicked), lng: parseFloat(lngClicked)}
        location.href = "#info-page";
    });

});

$$(document).on('click', 'a#save-store', function(event){
    // post data to api to save store
        findAddress();
        //reverse Geocoding

        // if location fails dont submit

            if(locationUndefined)
            {
                myApp.alert('You cannot submit store without location services, please enable them');
                mainView.router.back();
                return;
            }
            if(loggedIn != true)
            {
                myApp.alert('You must be logged in to add new store...');
                mainView.router.back();
                return;
            }
            // catch form values
            var coffee_name = $$("#store-name-input").val();
            var coffee_address = $$("#store-address-input").val();

            if(coffee_name.length < 3 || coffee_address<3)
            {
                myApp.alert('Please, enter store name and address...');
            }
            else
            {

                // show loader

                myApp.showPreloader('Saving store...');

                $$.ajax({
                    url: 'http://' + base_url + '/coffee_services/index.php/api/stores',
                    type: 'POST',
                    data: {
                        store_name: coffee_name,
                        store_latitude: defaultLatLng.lat,
                        store_longitude: defaultLatLng.lng,
                        store_address: coffee_address,
                        store_managed_by_user: userID
                    },
                    success: function(data) {
                        myApp.hidePreloader();
                        $$("#store-name-input").val('');
                        myApp.alert('Store is saved');
                        mainView.router.back();
                    },
                    fail: function() {
                        myApp.hidePreloader();
                        myApp.alert('Something went wrong, please try again later');
                        mainView.router.back();
                    }
                });
            }
});

$$( document ).on('pageshow', '#popup', function(event) {
    if(infoPopup == 1){
            $$("#review-popup").hide();
            $$("#add-coffee-popup").show();
            findAddress();
            //reverse Geocoding

            $$("#submit-coffee").unbind().on('click', function(event) {
                event.preventDefault();
                var coffee_name = $$("#coffee-name-input").val();
                $.ajax({
                    url: 'http://' + base_url + '/coffee_services/index.php/api/stores',
                    type: 'POST',
                    data: { store_name: coffee_name,
                            store_latitude: defaultLatLng.lat,
                            store_longitude: defaultLatLng.lng,
                            store_address: newAddress,
                            store_managed_by_user: userID
                            },
                })
                .done(function(data) {
                    $$("#coffee-name-input").val('');
                    location.href = "#map-page";
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

            });
        }
    else if(infoPopup == 2){
            $$("#add-coffee-popup").hide();
            $$("#review-popup").show();
            if(isReviewUpdate){
                $$("#review-input").val(currentReview.text);
                $$("#rating-input").val(currentReview.rating);
            }



            $$("#submit-review").unbind().on('click', function(event) {
                event.preventDefault();

                var review_data = {
                    review_text: $$("#review-input").val() ,
                    review_rating: $$("#rating-input").val(),
                    review_user_id: userID,
                    review_store_id: currentID,
                    is_update: isReviewUpdate
                }

                if(review_data.review_rating == '') return false;
                var json_review_data = JSON.stringify(review_data);

                $.ajax({
                    type: 'POST',
                    url: 'http://' + base_url + '/coffee_services/index.php/api/reviews',
                    dataType : "json",
                    contentType: "application/json; charset=utf-8",
                    data: json_review_data
                })
                .done(function(data) {
                    $$("#review-input").val('');
                    $$("#rating-input").val('');
                    location.href = "#info-page";
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

            });
         }

});

$$( document ).on( "pageshow", "#info-page", function() {
        var checkStoreID = '.' + currentID;
        console.log(checkStoreID);
        console.log(favoriteStores);

        console.log(favoriteStores.indexOf(checkStoreID));
        if(favoriteStores.indexOf(checkStoreID) > -1) $$("#coffee-favorites").prop('checked', true);
        else $$("#coffee-favorites").prop('checked', false);
        $.ajax({
            url: 'http://' + base_url + '/coffee_services/index.php/api/stores',
            type: 'GET',
            data: {id: currentID,
                user_id: userID},
        })
        .done(function(data) {

            var enableReview = true;

            $$("#coffee-id").text(data.store_id);
            $$("#coffee-distance").text(currentDistance + 'm');
            $$("#coffee-name").text(data.store_name);
            $$("#coffee-address").text(data.store_address);
            $$("#coffee-rating").text(data['rating']);
            $$("#review-text").html('');
            var storeReviews = data['reviews'];

            if(typeof storeReviews != 'undefined'){

            $.each(storeReviews, function(index, val) {

                if(val.review_type == 1) {
                   if(parseInt(val.review_user_id) == parseInt(userID)) {
                    $$("#review-text").append('<p>' + val.review_text + '<a href="#" id="edit-review-link" data-review-id=' + val.store_id + '> Edit</a></p>');
                    enableReview = false;
                    $$("#edit-review-link").bind('click', reviewData={text: val.review_text, rating: val.review_rating}, function(event) {
                        currentReview = reviewData;
                        isReviewUpdate = true;
                        infoPopup = 2;
                        location.href = "#popup";
                    });
                    }
                   else $$("#review-text").append('<p>' + val.review_text + '</p>');}
            });
            }
             $$("#rate-review").html('');

             //change back to (loggedIn && enableReview)

            if(loggedIn) {
                $$("#rate-review").append('<a href="#popup" data-role="button" data-rel="dialog" data-transition="pop" data-caffee-id="' + data.store_id + '" onclick="infoPopup=2; isReviewUpdate=false;">Add review &amp rate</a>');
            }

        })
        .fail(function() {
            console.log("error");
        });

        drawMap(defaultLatLng, true);  // No geolocation support, show default map

        $$("#coffee-favorites").unbind().on('click',  function() {
            var isChecked = ($$(this).is(':checked'));
            var favoritesData = {
                favorite_stores_change: isChecked,
                user_id: userID,
                favorite_stores: '.' + currentID
            }
            $.ajax({
                url: 'http://' + base_url + '/coffee_services/index.php/api/users',
                type: 'POST',
                dataType: 'json',
                data: favoritesData,
            })
            .done(function(data) {
                favoriteStores = data.favorite_stores;
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

        });

});


function drawMap(latlng, isInfo) {

    if (typeof latlng === 'undefined') latlng = {lat: 38.14, lng: -98.5};

    if (typeof isInfo === 'undefined')  isInfo = false;

    if (isInfo) var element = "small-map";
    else var element = "map-canvas";

    if(currentPage != 'map-page'){
        element = "location-map";
    }

    centerPosition = isInfo ? markerPosition : latlng;

    center = centerPosition;

    var myOptions = {
        zoom: 14,
        center: centerPosition,
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById(element), myOptions);

    map.setCenter(centerPosition);
        if(currentPage === 'map-page'){
            map.addListener('idle', function() {


                // show loader and then load ajax markers
                var bounds = map.getBounds();
                // set global center var
                center = bounds.getCenter();

                var ne = bounds.getNorthEast();

                // r = radius of the earth in statute miles
                var r = 3963.0;

                // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
                var lat1 = center.lat() / 57.2958;
                var lon1 = center.lng() / 57.2958;
                var lat2 = ne.lat() / 57.2958;
                var lon2 = ne.lng() / 57.2958;

                // distance = circle radius from center to Northeast corner of bounds
                var dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
                  Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));

                // set global radius var
                radius = dis;

                //myApp.alert('Load now found: ' + dis, 'Cofeine!');

                //myApp.showPreloader('Fetching locations!');

                loadStores();

                // window.setTimeout(function() {
                //     myApp.hidePreloader();
                // }, 3000);


            });
        }
    if(currentPage === 'new-store'){
        createMarker(map, defaultLatLng);
        findAddress();
    }

   // if(!isInfo){

   //      $$.ajax({
   //          url: 'http://' + base_url + '/coffee_services/index.php/api/stores',
   //          type: 'GET',
   //          data: {latitude: latlng.lat,
   //                 longitude:latlng.lng}
   //          ,
   //          success:function(data) {


   //              $$("#coffee-list").html('');
   //              $$.each(data, function(index, val) {
   //                  //distance in meters
   //                  var distanceString = getDistance({lat:val.store_latitude, lng: val.store_longitude });
   //                  var coffeeDistance = Math.round(val.distance*1000);
   //                  $$("#coffee-list").append('<a href="#" data-coffee-id="' + val.store_id + '"' + ' data-coffee-lat=' + val.store_latitude + ' data-coffee-lng=' + val.store_longitude + ' class="ui-btn ui-corner-all coffee-link">' + val.store_name + ' ' + distanceString + ' from you</a>');
   //                   var testLocation = [val.store_name, parseFloat(val.store_latitude), parseFloat(val.store_longitude), val.store_id, coffeeDistance];
   //                   createMarker(map, testLocation);
   //              });
   //          },
   //          error:function() {
   //              console.log("error");
   //          },
   //          complete:function() {
   //              console.log("complete");
   //          }
   //      });
   //  }
   // else{
   //      createMarker(map, centerPosition)
   // }
}


function loadStores(){
    // globals
    // radius
    // center
    // stores

    //find stores in radius arround center
    myApp.showPreloader('Loading new stores!');

    var lat = center.lat();
    var lng  = center.lng();

    $$.ajax({
        url: 'http://' + base_url + '/coffee_services/index.php/api/stores',
        type: 'GET',
        async: false,
        dataType: "json",
        jsonp: false,
        data: {
                latitude: lat,
                longitude: lng,
                radius:radius
            }
        ,
        success:function(respdata) {

            stores = respdata;
            console.log(respdata);
            //$$("#coffee-list").html('');
            $$.each(respdata, function(index, val) {


                var distanceString = getDistance({lat:val.store_latitude, lng: val.store_longitude });
                var coffeeDistance = Math.round(val.distance);

                if(index === 0)
                {
                    // fill pane with closest store
                    $$('div.info-pane-text').html('Closest store is ' + coffeeDistance + 'Km from you..');
                }

                // $$("#coffee-list").append('<a href="#" data-coffee-id="' + val.store_id + '"' + ' data-coffee-lat=' + val.store_latitude + ' data-coffee-lng=' + val.store_longitude + ' class="ui-btn ui-corner-all coffee-link">' + val.store_name + ' ' + distanceString + ' from you</a>');
                var testLocation = [val.store_name, parseFloat(val.store_latitude), parseFloat(val.store_longitude), val.store_id, coffeeDistance];
                createMarker(map, testLocation);
                 // console.log(coffeeDistance);

                fillList();

            });
            //alert(stores);
        },
        error:function(jqXHR, textStatus) {
            //console.log(textStatus);
            //alert(jqXHR);
        },
        complete:function() {
            //console.log(this.url);
        }
    });

    myApp.hidePreloader();

    console.log('loaded');

}

function fillList(){
    // clear list and rebuild it
    $$('ul#stores-list').html('');
    $$.each(stores, function(index, store){
            var listItem =
            '<li>' +
              '<a href="details.html" class="item-link item-content">' +
                '<div class="item-inner">' +
                  '<div class="item-title-row">' +
                    '<div class="item-title">' + store.store_name + '</div>' +
                    '<div class="item-after">' + store.store_country + '</div>' +
                  '</div>' +
                  '<div class="item-subtitle">' + store.store_attributes + '</div>' +
                  '<div class="item-text">' + store.store_address + '</div>' +
                '</div>' +
              '</a>' +
            '</li>'
        $$('ul#stores-list').append(listItem);
    });
}

function createMarker(map, location){
    if (location.length == 5){
            var marker = new google.maps.Marker({
            position: {lat: location[1], lng: location[2]},
            map: map,
            url: '#info-page',
            title: location[0]
        });

        console.log('created');

        marker.set("id", location[3]);
        marker.set("distance", location[4]);

        marker.addListener('click', function() {
            console.log('clicled');
            currentID = marker.get("id");
            currentDistance = marker.get("distance");
            markerPosition = {lat: marker.getPosition().lat(), lng: marker.getPosition().lng()};
            //window.location.href = marker.url;
            mainView.router.loadPage('details.html');
          });
    }
    else{
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
    }
}

function findAddress(){
    var geocoder_address = new google.maps.Geocoder();
    geocoder_address.geocode({'location':defaultLatLng}, function(results, status){
        if (status === google.maps.GeocoderStatus.OK){
            saveNewAddress(results[0]);
        }
    });
}

function saveNewAddress(address){
console.log(address);
    newAddress = address.formatted_address;
    $$('#store-address-input').val(newAddress);
    $$('#store-zip-input').val(address.address_components[address.address_components.length - 1].long_name);
}

function getStores(userID, getStoresType){
    if (getStoresType == 'favorite') $$("#favorite-list").html('');
    else if (getStoresType == 'visited') $$("#visited-list").html('');
    $.ajax({
        url: 'http://' + base_url + '/coffee_services/index.php/api/users',
        type: 'GET',
        data: {id: userID},
    })
    .done(function(data) {
        if (getStoresType == 'favorite') getStoresData(data.favorite_stores);
        else if (getStoresType == 'visited') getStoresData(data.visited_stores);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });

}

function getStoresData(storesArray){
    $.ajax({
        url: 'http://' + base_url + '/coffee_services/index.php/api/stores',
        type: 'GET',
        data: {ids: storesArray},
    })
    .done(function(data) {
        $.each(data, function(index, store) {
            //var coffeeDistance = Math.round(coffee.distance*1000);
            if (getStoresType == 'favorite') var elementList = $$("#favorite-list");
            else if (getStoresType == 'visited') var elementList = $$("#visited-list");
            elementList.append('<a href="#" data-coffee-id="' + store.store_id + '"' + ' data-coffee-lat=' + store.store_latitude + ' data-coffee-lng=' + store.store_longitude + ' class="ui-btn ui-corner-all coffee-link">' + store.store_name + '  ' + getDistance({lat: store.store_latitude, lng: store.store_longitude}) + ' from you</a>');
        });
        $$(".ui-btn.ui-corner-all.coffee-link").on('click', function(event) {
            event.preventDefault();
            var calcLatLng = {lat: $$(this).data("coffee-lat"), lng: $$(this).data("coffee-lng")};
            console.log(getDistance(calcLatLng));
            // currentID = $$(this).data("coffee-id");
            // var latClicked = $$(this).data("coffee-lat");
            // var lngClicked = $$(this).data("coffee-lng");
            // markerPosition = {lat: parseFloat(latClicked), lng: parseFloat(lngClicked)}
            // location.href = "#info-page";
        });
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function getDistance(latlng){

    var distance = Math.round(1000* 3959 * Math.acos( Math.cos( toRadians(defaultLatLng.lat) ) * Math.cos( toRadians( latlng.lat ) ) * Math.cos( toRadians( latlng.lng ) - toRadians(defaultLatLng.lng) ) + Math.sin( toRadians(defaultLatLng.lat) ) * Math.sin( toRadians( latlng.lat ) ) ) );
    if (distance > 1000) return Math.round(distance/1000) + 'km';
    else return distance + 'm';
}

function toRadians(degrees){
    return degrees * Math.PI / 180;
}
